import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductoDescripcion } from 'src/app/interfaces/producto-descripcion.interface';
import { ProductosService } from 'src/app/services/productos.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  producto: ProductoDescripcion = {};
  productoID: undefined;

  constructor( private route: ActivatedRoute,
               public productoService: ProductosService ) { }

  ngOnInit() {
    
    this.route.params
        .subscribe( parametros => {
          console.log(parametros['productoID']);

          this.productoService.getProducto(parametros['productoID'])
                .subscribe( (producto: ProductoDescripcion ) => {
                  this.productoID = parametros['productoID'];
                  //console.log(producto);
                  this.producto = producto;

                });

        });

  }

}
