
export interface InfoPagina {
  titulo?: string;
  email?: string;
  nombre_corto?: string;
  twitch?: string;
  instagram?: string;
  equipo_trabajo?: any[];
}